/*!
A map from a sparse set of clustered indices to a range of indices
*/
#![forbid(unsafe_code, missing_docs, missing_debug_implementations)]

use num_traits::{One, SaturatingAdd, Zero};
use smallvec::SmallVec;
use std::ops::Sub;

mod stack;
pub use stack::*;

/// A surjective, increasing mapping from a range `a..b` to `0..c`
#[derive(Debug, Clone, Eq, Hash)]
pub struct SparseIndex<I, const N: usize> {
    segments: SmallVec<[Segment<I>; N]>,
}

impl<I, const N: usize> SparseIndex<I, N>
where
    I: SaturatingAdd + Sub<Output = I> + Zero + One + Ord + Copy,
{
    /// Create a new, empty map
    ///
    /// # Example
    /// ```rust
    /// # use sparse_index::SparseIndex;
    /// let mut ix = SparseIndex::<u64, 1>::new();
    /// assert!(ix.is_empty());
    /// assert_eq!(ix.no_segments(), 0);
    /// ix.push_segment(1, 2);
    /// assert!(!ix.is_empty());
    /// assert_eq!(ix.no_segments(), 1);
    /// ```
    #[inline(always)]
    pub fn new() -> Self {
        SparseIndex {
            segments: SmallVec::default(),
        }
    }

    /// Get whether this map is empty
    ///
    /// Is true if and only if `self.no_segments() == 0`
    ///
    /// # Example
    /// ```rust
    /// # use sparse_index::SparseIndex;
    /// let mut ix = SparseIndex::<i32, 1>::new();
    /// assert!(ix.is_empty());
    /// assert_eq!(ix.no_segments(), 0);
    /// ix.push_segment(-5, 3);
    /// assert!(!ix.is_empty());
    /// assert_ne!(ix.no_segments(), 0);
    /// ix.clear();
    /// assert!(ix.is_empty());
    /// assert_eq!(ix.no_segments(), 0);
    /// ```
    #[inline(always)]
    pub fn is_empty(&self) -> bool {
        self.segments.is_empty()
    }

    /// Get the number of segments in this map
    #[inline(always)]
    pub fn no_segments(&self) -> usize {
        self.segments.len()
    }

    /// Get the segment capacity of this map
    #[inline(always)]
    pub fn capacity(&self) -> usize {
        self.segments.capacity()
    }

    /// Get capacity for `s` additional segments
    #[inline(always)]
    pub fn reserve(&mut self, s: usize) {
        self.segments.reserve(s)
    }

    /// Clear this map, preserving it's capacity and setting the beginning to `begin`
    ///
    /// # Example
    /// ```rust
    /// # use sparse_index::SparseIndex;
    /// let mut ix = SparseIndex::<i32, 1>::new();
    /// assert!(ix.is_empty());
    /// assert_eq!(ix.no_segments(), 0);
    /// assert_eq!(ix.capacity(), 1);
    /// ix.push_segment(-5, 3);
    /// ix.push_segment(0, 3);
    /// assert!(!ix.is_empty());
    /// assert_eq!(ix.no_segments(), 2);
    /// let old_capacity = ix.capacity();
    /// assert!(old_capacity >= 2);
    /// ix.clear();
    /// assert!(ix.is_empty());
    /// assert_eq!(ix.no_segments(), 0);
    /// assert_eq!(ix.capacity(), old_capacity);
    /// ```
    #[inline(always)]
    pub fn clear(&mut self) {
        self.segments.clear();
    }

    /// If `self.get(ix) < self.get(ix + 1)`, return `Ok(self.get(ix))`, else return `Err(self.get(ix))`.
    ///
    /// # Example
    /// ```rust
    /// # use sparse_index::SparseIndex;
    /// let mut ix = SparseIndex::<i32, 1>::new();
    /// ix.push_segment(-5, 2);
    /// assert_eq!(ix.get_step(-6), Err(0));
    /// assert_eq!(ix.get_step(-5), Ok(0));
    /// assert_eq!(ix.get_step(-4), Ok(1));
    /// assert_eq!(ix.get_step(-3), Err(2));
    /// assert_eq!(ix.get_step(-2), Err(2));
    /// ix.push_segment(-2, 2);
    /// assert_eq!(ix.get_step(-4), Ok(1));
    /// assert_eq!(ix.get_step(-3), Err(2));
    /// assert_eq!(ix.get_step(-2), Ok(2));
    /// assert_eq!(ix.get_step(-1), Ok(3));
    /// assert_eq!(ix.get_step(0), Err(4));
    /// assert_eq!(ix.get_step(1), Err(4));
    /// ```
    #[inline(always)]
    pub fn get_step(&self, ix: I) -> Result<I, I> {
        self.borrowed().get_step(ix)
    }

    /// Get the index mapped to by `ix`
    ///
    /// # Example
    /// ```rust
    /// # use sparse_index::SparseIndex;
    /// let mut ix = SparseIndex::<i32, 1>::new();
    /// ix.push_segment(7, 1);
    /// ix.push_segment(9, 3);
    /// assert_eq!(ix.get(6), 0);
    /// assert_eq!(ix.get(7), 0);
    /// assert_eq!(ix.get(8), 1);
    /// assert_eq!(ix.get(9), 1);
    /// assert_eq!(ix.get(10), 2);
    /// assert_eq!(ix.get(11), 3);
    /// assert_eq!(ix.get(12), 4);
    /// assert_eq!(ix.get(13), 4);
    /// ```
    #[inline]
    pub fn get(&self, ix: I) -> I {
        self.get_step(ix).unwrap_or_else(|ix| ix)
    }

    /// Get the index of the segment potentially containing a given element
    ///
    /// # Example
    /// ```rust
    /// # use sparse_index::SparseIndex;
    /// let mut ix = SparseIndex::<i32, 1>::new();
    /// ix.push_segment(7, 1);
    /// ix.push_segment(9, 3);
    /// assert_eq!(ix.segment(6), None);
    /// assert_eq!(ix.segment(7), Some(0));
    /// assert_eq!(ix.segment(8), Some(0));
    /// assert_eq!(ix.segment(9), Some(1));
    /// ```
    #[inline(always)]
    pub fn segment(&self, elem: I) -> Option<usize> {
        self.borrowed().segment(elem)
    }

    /// Remove all mappings from elements greater than `max`
    ///
    /// # Example
    /// ```rust
    /// # use sparse_index::SparseIndex;
    /// let mut ix = SparseIndex::<i32, 1>::new();
    /// ix.push_segment(-5, 2);
    /// ix.push_segment(-1, 3);
    /// ix.push_segment(4, 2);
    /// assert_eq!(ix.src_max(), Some(6));
    /// let mut smaller_ix = SparseIndex::<i32, 1>::new();
    /// smaller_ix.push_segment(-5, 2);
    /// smaller_ix.push_segment(-1, 1);
    /// assert_eq!(smaller_ix.src_max(), Some(0));
    /// assert_ne!(ix, smaller_ix);
    /// ix.truncate_src(0);
    /// assert_eq!(ix.src_max(), Some(0));
    /// assert_eq!(ix, smaller_ix);
    /// ```
    #[inline]
    pub fn truncate_src(&mut self, max: I) {
        loop {
            if self.segments.is_empty() {
                return;
            }
            let last = self.segments.len() - 1;
            let begin_src = self.begin_src(last);
            if begin_src >= max {
                self.segments.pop();
                continue;
            }
            self.segments[last].end_trg = self.begin_trg(last).saturating_add(&(max - begin_src));
            return;
        }
    }

    /// Remove all mappings to elements greater than or equal to `max`
    ///
    /// # Example
    /// ```rust
    /// # use sparse_index::SparseIndex;
    /// let mut ix = SparseIndex::<i32, 1>::new();
    /// ix.push_segment(-5, 2);
    /// ix.push_segment(-1, 3);
    /// ix.push_segment(4, 2);
    /// let mut smaller_ix = SparseIndex::<i32, 1>::new();
    /// smaller_ix.push_segment(-5, 2);
    /// smaller_ix.push_segment(-1, 2);
    /// assert_ne!(ix, smaller_ix);
    /// ix.truncate_trg(4);
    /// assert_eq!(ix, smaller_ix);
    /// ```
    #[inline]
    pub fn truncate_trg(&mut self, max: I) {
        loop {
            if self.segments.is_empty() {
                return;
            }
            let last = self.segments.len() - 1;
            if self.begin_trg(last) >= max {
                self.segments.pop();
                continue;
            }
            self.segments[last].end_trg = max;
            return;
        }
    }

    /// Map `i..(i + len)` to `c..(c + len)`, where `c = self.trg_max()` before this function is called (`c + len` will be the new value).
    ///
    /// Returns `Err(())` if `!self.is_empty() && !n.is_zero() && i < self.src_max().unwrap() - self.len_trg(self.no_segments() - 1)`, leaving the map unchanged;
    /// in other words, all segments inserted into this map must be inserted in *sorted* order.
    ///
    /// # Examples
    /// ```rust
    /// # use sparse_index::SparseIndex;
    /// let mut ix = SparseIndex::<u64, 1>::new();
    /// assert!(ix.is_empty());
    /// assert_eq!(ix.push_segment(2, 5), Ok(()));
    /// assert!(!ix.is_empty());
    /// let old_ix = ix.clone();
    /// assert_eq!(ix.push_segment(1, 5), Err(()));
    /// assert_eq!(ix, old_ix);
    /// ```
    #[inline]
    pub fn push_segment(&mut self, i: I, len: I) -> Result<(), ()> {
        if len.is_zero() {
            return Ok(());
        }
        if self.segments.is_empty() {
            self.segments.push(Segment {
                begin_src: i,
                end_trg: len,
            });
            return Ok(());
        }
        let last = self.segments.len() - 1;
        let begin_src = self.segments[last].begin_src;
        let end_trg = self.segments[last].end_trg;
        let end_src = self.end_src(last);
        if i < begin_src {
            return Err(());
        }
        if i > end_src {
            self.segments.push(Segment {
                begin_src: i,
                end_trg: end_trg.saturating_add(&len),
            })
        } else {
            let len = len.saturating_add(&(i - begin_src));
            self.segments[last].end_trg = self.begin_trg(last).saturating_add(&len);
        }
        Ok(())
    }

    /// A helper for implementing sparse stacks: maps `i..(i + n)` to `c..(c + n)`, where `i = self.src_max().unwrap_or(base)` and `c = self.trg_max()` before this function is called.
    #[inline]
    pub fn extend(&mut self, n: I, base: I) {
        self.push_segment(self.src_max().unwrap_or(base), n)
            .unwrap();
    }

    /// A helper for implementing sparse stacks: sets `i = self.src_max().unwrap_or(base)`, removes all mappings to elements greater than or equal to `len`, and then
    /// pushes the segment `i..i + n` to `c..c + n`, where `c = len.min(self.trg_max())`.
    #[inline]
    pub fn push_some(&mut self, len: I, n: I, base: I) {
        let old_trg_max = self.trg_max();
        let i = self.src_max().unwrap_or(base);
        self.truncate_trg(len);
        debug_assert!(self.trg_max() == old_trg_max.min(len));
        self.push_segment(i, n).unwrap();
    }

    /// Pop one element off the final run, returning the index popped, or `None` if the stack was empty
    #[inline]
    pub fn pop(&mut self) -> Option<I> {
        let last = self.segments.last_mut()?;
        let popped = last.end_trg - I::one();
        last.end_trg = popped;
        if last.begin_src == popped {
            self.segments.pop();
        }
        Some(popped)
    }

    /// A helper for implementing sparse stacks: pop one element off the final run, re-setting the length to the previous max target minus one afterwards
    #[inline]
    pub fn pop_len(&mut self, base: I) -> Option<I> {
        let popped = self.pop()?;
        let max = self.trg_max();
        if max != popped {
            debug_assert!(max < popped);
            self.extend(max - popped, base)
        }
        Some(popped)
    }

    /// Get the minimum element of the source range. All elements below this element will be mapped to `I::zero`
    ///
    /// In the case of the empty map, return `None`
    #[inline]
    pub fn src_min(&self) -> Option<I> {
        if self.segments.is_empty() {
            return None;
        }
        Some(self.begin_src(0))
    }

    /// Get the maximum element of the source range. All elements above this element will be mapped to `self.trg_max()`
    ///
    /// In the case of the empty map, return `None`
    #[inline(always)]
    pub fn src_max(&self) -> Option<I> {
        self.borrowed().src_max()
    }

    /// Get the maximum element of the target range.
    #[inline(always)]
    pub fn trg_max(&self) -> I {
        self.borrowed().trg_max()
    }

    /// Get this map as a list of segments
    #[inline(always)]
    pub fn segments(&self) -> &[Segment<I>] {
        &self.segments[..]
    }

    /// Get the beginning of a segment's target
    ///
    /// Panics if `ix >= self.segments().len()`
    #[inline(always)]
    pub fn begin_trg(&self, ix: usize) -> I {
        self.borrowed().begin_trg(ix)
    }

    /// Get the end of a segment's target
    ///
    /// Panics if `ix >= self.segments().len()`
    #[inline(always)]
    pub fn end_trg(&self, ix: usize) -> I {
        self.segments[ix].end_trg
    }

    /// Get the beginning of a segment's source
    ///
    /// Panics if `ix >= self.segments().len()`
    #[inline(always)]
    pub fn begin_src(&self, ix: usize) -> I {
        self.segments[ix].begin_src
    }

    /// Get the end of a segment's source
    ///
    /// Panics if `ix >= self.segments().len()`
    #[inline(always)]
    pub fn end_src(&self, ix: usize) -> I {
        self.borrowed().end_src(ix)
    }

    /// Get the length of a segment's source
    ///
    /// Panics if `ix >= self.segments().len()`
    #[inline(always)]
    pub fn len_src(&self, ix: usize) -> I {
        self.borrowed().len_src(ix)
    }

    /// Get the length of a segment's target
    ///
    /// Panics if `ix >= self.segments().len()`
    #[inline(always)]
    pub fn len_trg(&self, ix: usize) -> I {
        self.borrowed().len_trg(ix)
    }

    /// Check whether the invariants of this map hold. This should always be `true`
    #[inline(always)]
    pub fn check_invariants(&self) -> bool {
        self.borrowed().check_invariants()
    }
}

/// A borrowed  form if [`SparseIndex`]
#[derive(Debug, Copy, Clone, Eq, Hash)]
pub struct SparseIndexRef<'a, I> {
    segments: &'a [Segment<I>],
}

impl<I> SparseIndexRef<'_, I>
where
    I: SaturatingAdd + Sub<Output = I> + Zero + Ord + Copy,
{
    /// Get whether this map is empty
    ///
    /// Is true if and only if `self.no_segments() == 0`
    ///
    /// # Example
    /// ```rust
    /// # use sparse_index::SparseIndex;
    /// let mut ix = SparseIndex::<i32, 1>::new();
    /// assert!(ix.is_empty());
    /// assert_eq!(ix.no_segments(), 0);
    /// ix.push_segment(-5, 3);
    /// assert!(!ix.is_empty());
    /// assert_ne!(ix.no_segments(), 0);
    /// ix.clear();
    /// assert!(ix.is_empty());
    /// assert_eq!(ix.no_segments(), 0);
    /// ```
    #[inline(always)]
    pub fn is_empty(&self) -> bool {
        self.segments.is_empty()
    }

    /// Get the number of segments in this map
    #[inline(always)]
    pub fn no_segments(&self) -> usize {
        self.segments.len()
    }

    /// If `self.get(ix) < self.get(ix + 1)`, return `Ok(self.get(ix))`, else return `Err(self.get(ix))`.
    #[inline]
    pub fn get_step(&self, ix: I) -> Result<I, I> {
        let segment_ix = self.segment(ix).ok_or(I::zero())?;
        let begin_src = self.begin_src(segment_ix);
        debug_assert!(ix >= begin_src);
        let begin_trg = self.begin_trg(segment_ix);
        let ix = begin_trg + (ix - begin_src);
        let end_trg = self.end_trg(segment_ix);
        if ix >= end_trg {
            Err(end_trg)
        } else {
            Ok(ix)
        }
    }

    /// Get the index mapped to by `ix`
    #[inline]
    pub fn get(&self, ix: I) -> I {
        self.get_step(ix).unwrap_or_else(|ix| ix)
    }

    /// Get the index of the segment potentially containing a given element
    ///
    /// # Example
    /// ```rust
    /// # use sparse_index::SparseIndex;
    /// let mut ix = SparseIndex::<i32, 1>::new();
    /// ix.push_segment(7, 1);
    /// ix.push_segment(9, 3);
    /// assert_eq!(ix.segment(6), None);
    /// assert_eq!(ix.segment(7), Some(0));
    /// assert_eq!(ix.segment(8), Some(0));
    /// assert_eq!(ix.segment(9), Some(1));
    /// ```
    #[inline]
    pub fn segment(&self, elem: I) -> Option<usize> {
        self.segments
            .binary_search_by_key(&elem, |segment| segment.begin_src)
            .map(Some)
            .unwrap_or_else(|ix| ix.checked_sub(1))
    }

    /// Get the minimum element of the source range. All elements below this element will be mapped to `I::zero`
    ///
    /// In the case of the empty map, return `None`
    #[inline]
    pub fn src_min(&self) -> Option<I> {
        if self.segments.is_empty() {
            return None;
        }
        Some(self.begin_src(0))
    }

    /// Get the maximum element of the source range. All elements above this element will be mapped to `self.trg_max()`
    ///
    /// In the case of the empty map, return `None`
    #[inline]
    pub fn src_max(&self) -> Option<I> {
        if self.segments.is_empty() {
            return None;
        }
        Some(self.end_src(self.segments.len() - 1))
    }

    /// Get the maximum element of the target range.
    #[inline(always)]
    pub fn trg_max(&self) -> I {
        if self.segments.is_empty() {
            return I::zero();
        }
        self.segments.last().unwrap().end_trg
    }
    /// Get this map as a list of segments
    #[inline(always)]
    pub fn segments(&self) -> &[Segment<I>] {
        &self.segments[..]
    }

    /// Get the beginning of a segment's target
    ///
    /// Panics if `ix >= self.segments().len()`
    #[inline(always)]
    pub fn begin_trg(&self, ix: usize) -> I {
        if ix == 0 {
            return I::zero();
        }
        self.segments[ix - 1].end_trg
    }

    /// Get the end of a segment's target
    ///
    /// Panics if `ix >= self.segments().len()`
    #[inline(always)]
    pub fn end_trg(&self, ix: usize) -> I {
        self.segments[ix].end_trg
    }

    /// Get the beginning of a segment's source
    ///
    /// Panics if `ix >= self.segments().len()`
    #[inline(always)]
    pub fn begin_src(&self, ix: usize) -> I {
        self.segments[ix].begin_src
    }

    /// Get the end of a segment's source
    ///
    /// Panics if `ix >= self.segments().len()`
    #[inline]
    pub fn end_src(&self, ix: usize) -> I {
        self.segments[ix].begin_src + (self.end_trg(ix) - self.begin_trg(ix))
    }

    /// Get the length of a segment's source
    ///
    /// Panics if `ix >= self.segments().len()`
    #[inline]
    pub fn len_src(&self, ix: usize) -> I {
        self.end_src(ix) - self.begin_src(ix)
    }

    /// Get the length of a segment's target
    ///
    /// Panics if `ix >= self.segments().len()`
    #[inline]
    pub fn len_trg(&self, ix: usize) -> I {
        self.end_trg(ix) - self.begin_trg(ix)
    }

    /// Check whether the invariants of this map hold. This should always be `true`
    #[inline]
    pub fn check_invariants(&self) -> bool {
        //TODO: check more invariants...
        (!self.trg_max().is_zero() || self.is_empty())
            && (self.is_empty() || !self.segments[0].end_trg.is_zero())
            && self.segments.windows(2).all(|windows| {
                windows[0].begin_src < windows[1].begin_src
                    && windows[0].end_trg < windows[1].end_trg
                    && windows[1].begin_src - windows[0].begin_src
                        > windows[1].end_trg - windows[0].end_trg
            })
    }
}

impl<I, const N: usize> SparseIndex<I, N> {
    /// Get this map as a borrowed `SparseIndexRef`
    #[inline(always)]
    pub fn borrowed(&self) -> SparseIndexRef<I> {
        SparseIndexRef {
            segments: &self.segments[..],
        }
    }
}

impl<I, J, const N: usize, const M: usize> PartialEq<SparseIndex<I, N>> for SparseIndex<J, M>
where
    J: PartialEq<I>,
{
    #[inline(always)]
    fn eq(&self, other: &SparseIndex<I, N>) -> bool {
        self.borrowed().eq(&other.borrowed())
    }
}

impl<I, J, const N: usize> PartialEq<SparseIndex<I, N>> for SparseIndexRef<'_, J>
where
    J: PartialEq<I>,
{
    #[inline(always)]
    fn eq(&self, other: &SparseIndex<I, N>) -> bool {
        self.eq(&other.borrowed())
    }
}

impl<I, J, const N: usize> PartialEq<SparseIndexRef<'_, I>> for SparseIndex<J, N>
where
    J: PartialEq<I>,
{
    #[inline(always)]
    fn eq(&self, other: &SparseIndexRef<'_, I>) -> bool {
        self.borrowed().eq(other)
    }
}

impl<I, J> PartialEq<SparseIndexRef<'_, I>> for SparseIndexRef<'_, J>
where
    J: PartialEq<I>,
{
    #[inline]
    fn eq(&self, other: &SparseIndexRef<'_, I>) -> bool {
        self.segments.len() == other.segments.len()
            && self
                .segments
                .iter()
                .zip(other.segments.iter())
                .all(|(this, other)| this == other)
    }
}

/// A segment in a [`SparseIndex`] map
#[derive(Debug, Copy, Clone, Eq, Hash)]
pub struct Segment<I> {
    /// The end index of this segment in the source range
    pub begin_src: I,
    /// The beginning index of this segment in the target range
    pub end_trg: I,
}

impl<I, J> PartialEq<Segment<I>> for Segment<J>
where
    J: PartialEq<I>,
{
    #[inline]
    fn eq(&self, other: &Segment<I>) -> bool {
        self.begin_src == other.begin_src && self.end_trg == other.end_trg
    }
}

#[cfg(test)]
mod tests {
    use crate::SparseIndex;

    #[test]
    fn empty_mapping() {
        let mut empty = SparseIndex::<u64, 74>::new();
        assert!(empty.is_empty());
        assert!(empty.check_invariants());
        assert_eq!(empty.segments(), []);
        assert_eq!(empty.no_segments(), 0);
        assert_eq!(empty.capacity(), 74);
        assert_eq!(empty.src_min(), None);
        assert_eq!(empty.src_max(), None);
        assert_eq!(empty.trg_max(), 0);
        for i in 0..255 {
            assert_eq!(empty.get(i), 0);
            assert_eq!(empty.get_step(i), Err(0));
            assert_eq!(empty.segment(i), None);
        }
        empty.push_segment(5, 0).unwrap();
        assert!(empty.is_empty());
        empty.push_segment(3, 0).unwrap();
        assert!(empty.is_empty());
        let mut nonempty = empty;
        nonempty.push_segment(2, 1).unwrap();
        assert!(!nonempty.is_empty());
        nonempty.truncate_trg(0);
        assert!(nonempty.is_empty());
    }

    #[test]
    fn simple_mapping() {
        let target_mapping = [0, 1, 2, 3, 4, 5, 5, 5, 6, 6, 7, 7, 7, 8, 8, 9];
        let mut ix = SparseIndex::<i32, 2>::new();
        for (i, window) in target_mapping.windows(2).enumerate() {
            assert_eq!(ix.get(i as i32), window[0]);
            assert_eq!(ix.get(i as i32 + 1), window[0]);
            assert_eq!(ix.get_step(i as i32), Err(window[0]));
            assert_eq!(ix.get_step(i as i32 + 1), Err(window[0]));
            if window[1] > window[0] {
                ix.push_segment(i as i32, 1).unwrap();
                assert!(ix.check_invariants());
                assert_eq!(ix.get_step(i as i32), Ok(window[0]));
                assert_eq!(ix.get_step(i as i32 + 1), Err(window[1]));
            } else {
                // Just double checking the test itself...
                assert_eq!(window[1], window[0]);
            }
            assert_eq!(ix.get(i as i32), window[0]);
            assert_eq!(ix.get(i as i32 + 1), window[1]);
        }
        assert_eq!(ix.src_min(), Some(0));
        assert_eq!(ix.src_max(), Some(15));
        assert_eq!(ix.trg_max(), 9);

        for (i, &target) in target_mapping.iter().enumerate() {
            assert_eq!(ix.get(i as i32), target);
            assert_eq!(
                ix.get_step(i as i32),
                match target_mapping.get(i + 1) {
                    Some(&m) if m != target => Ok(target),
                    _ => Err(target),
                }
            )
        }

        ix.truncate_src(8);
        assert!(ix.check_invariants());
        assert_eq!(ix.src_min(), Some(0));
        assert_eq!(ix.src_max(), Some(8));
        assert_eq!(ix.trg_max(), 6);
        assert_eq!(ix.get_step(8), Err(6));
        for (i, &target) in target_mapping[0..8].iter().enumerate() {
            assert_eq!(ix.get(i as i32), target);
            assert_eq!(
                ix.get_step(i as i32),
                //TODO: think about this
                match target_mapping[0..9].get(i + 1) {
                    Some(&m) if m != target => Ok(target),
                    _ => Err(target),
                }
            )
        }
        for i in 8..16 {
            assert_eq!(ix.get_step(i as i32), Err(target_mapping[9]));
        }

        ix.truncate_trg(5);
        assert!(ix.check_invariants());
        assert_eq!(ix.src_min(), Some(0));
        assert_eq!(ix.src_max(), Some(5));
        assert_eq!(ix.trg_max(), 5);
        assert_eq!(ix.get_step(5), Err(5));
        for (i, &target) in target_mapping[0..5].iter().enumerate() {
            assert_eq!(ix.get(i as i32), target);
            assert_eq!(
                ix.get_step(i as i32),
                //TODO: think about this
                match target_mapping[0..6].get(i + 1) {
                    Some(&m) if m != target => Ok(target),
                    _ => Err(target),
                }
            )
        }
        for i in 5..16 {
            assert_eq!(ix.get_step(i as i32), Err(target_mapping[5]));
        }

        ix.extend(11, 0);
        assert!(ix.check_invariants());
        assert_eq!(ix.src_min(), Some(0));
        assert_eq!(ix.src_max(), Some(16));
        assert_eq!(ix.trg_max(), 16);
        for i in 0..16 {
            assert_eq!(ix.get_step(i), Ok(i))
        }
        for i in 16..32 {
            assert_eq!(ix.get_step(i), Err(16))
        }
    }

    #[test]
    fn extreme_mappings() {
        // Extreme u8 mappings
        let mut u8_ix = SparseIndex::<u8, 8>::new();
        u8_ix.push_segment(0, 255).unwrap();
        assert_eq!(u8_ix.no_segments(), 1);
        for i in 0..255 {
            assert_eq!(u8_ix.get_step(i), Ok(i));
        }
        assert_eq!(u8_ix.get_step(255), Err(255));
        assert_eq!(u8_ix.src_min(), Some(0));
        assert_eq!(u8_ix.src_max(), Some(255));
        assert_eq!(u8_ix.trg_max(), 255);
        assert_eq!(u8_ix.len_src(0), 255);
        assert_eq!(u8_ix.len_trg(0), 255);

        // This doesn't panic, but doesn't change anything, since the semantics of a SparseIndex are purely value-based
        let old_u8_ix = u8_ix.clone();
        u8_ix.push_segment(255, 1).unwrap();
        assert_eq!(u8_ix, old_u8_ix);

        // The behaviour is the same if we construct the massive range differently
        let mut u8_ix_ = SparseIndex::<u8, 16>::new();
        u8_ix_.push_segment(0, 128).unwrap();
        assert_eq!(u8_ix_.no_segments(), 1);
        for i in 0..128 {
            assert_eq!(u8_ix_.get_step(i), Ok(i));
        }
        for i in 128..=255 {
            assert_eq!(u8_ix_.get_step(i), Err(128));
        }
        assert_eq!(u8_ix_.src_min(), Some(0));
        assert_eq!(u8_ix_.src_max(), Some(128));
        assert_eq!(u8_ix_.trg_max(), 128);
        assert_eq!(u8_ix_.len_src(0), 128);
        assert_eq!(u8_ix_.len_trg(0), 128);

        u8_ix_.push_segment(100, 200).unwrap();
        assert_eq!(u8_ix_.no_segments(), 1);
        assert_eq!(u8_ix, u8_ix_);

        // Extreme u64 mappings
        let mut u64_ix = SparseIndex::<u64, 1>::new();
        u64_ix.push_segment(0, u64::MAX).unwrap();
        assert_eq!(u64_ix.no_segments(), 1);
        for i in 0..255 {
            assert_eq!(u64_ix.get_step(i), Ok(i));
        }
        for i in (u64::MAX - 255)..u64::MAX {
            assert_eq!(u64_ix.get_step(i), Ok(i));
        }
        assert_eq!(u64_ix.get_step(u64::MAX), Err(u64::MAX));
        assert_eq!(u64_ix.src_min(), Some(0));
        assert_eq!(u64_ix.src_max(), Some(u64::MAX));
        assert_eq!(u64_ix.trg_max(), u64::MAX);
        assert_eq!(u64_ix.len_src(0), u64::MAX);
        assert_eq!(u64_ix.len_trg(0), u64::MAX);

        // Again, this doesn't panic, but doesn't change anything, since the semantics of a SparseIndex are purely value-based
        let old_u64_ix = u64_ix.clone();
        u64_ix.push_segment(u64::MAX, 1).unwrap();
        assert_eq!(u64_ix, old_u64_ix);

        // Again, the behaviour is the same if we construct the massive range differently
        let mut u64_ix_ = SparseIndex::<u64, 3>::new();
        u64_ix_.push_segment(0, u64::MAX - 256).unwrap();
        assert_eq!(u64_ix_.no_segments(), 1);
        for i in 0..128 {
            assert_eq!(u64_ix_.get_step(i), Ok(i));
        }
        for i in (u64::MAX - 512)..(u64::MAX - 256) {
            assert_eq!(u64_ix_.get_step(i), Ok(i));
        }
        for i in (u64::MAX - 256)..=u64::MAX {
            assert_eq!(u64_ix_.get_step(i), Err(u64::MAX - 256));
        }
        assert_eq!(u64_ix_.src_min(), Some(0));
        assert_eq!(u64_ix_.src_max(), Some(u64::MAX - 256));
        assert_eq!(u64_ix_.trg_max(), u64::MAX - 256);
        assert_eq!(u64_ix_.len_src(0), u64::MAX - 256);
        assert_eq!(u64_ix_.len_trg(0), u64::MAX - 256);

        u64_ix_.push_segment(1000, u64::MAX - 512).unwrap();
        assert_eq!(u64_ix_.no_segments(), 1);
        assert_eq!(u64_ix, u64_ix_);
    }
}

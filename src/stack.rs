/*!
A sparse stack data structure implemented using [`SparseIndex`]
*/
use std::fmt::Debug;
use std::hash::Hash;

use smallvec::Array;

use super::*;

/// A sparse stack data structure implemented using [`SparseIndex`]
pub struct SparseStack<A: Array> {
    /// The elements of this sparse stack
    elems: SmallVec<A>,
    /// The indices of this sparse stack
    ix: SparseIndex<usize, 1>,
}

impl<A> SparseStack<A>
where
    A: Array,
{
    /// Create a new, empty sparse stack
    #[inline]
    pub fn new() -> SparseStack<A> {
        SparseStack {
            elems: SmallVec::new(),
            ix: SparseIndex::new(),
        }
    }

    /// Get whether this sparse stack is empty
    #[inline(always)]
    pub fn is_empty(&self) -> bool {
        self.ix.is_empty()
    }

    /// Get the length of this sparse stack
    #[inline(always)]
    pub fn len(&self) -> usize {
        self.ix.trg_max()
    }

    /// Get the number of non-`None` elements in this sparse stack
    #[inline(always)]
    pub fn no_elems(&self) -> usize {
        self.elems.len()
    }

    /// Get the number of `None` elements in this sparse stack
    #[inline(always)]
    pub fn no_gaps(&self) -> usize {
        self.len() - self.no_elems()
    }

    /// Get an element of this stack, or `None` if `ix` is out of bounds
    #[inline]
    pub fn get(&self, ix: usize) -> Option<Option<&A::Item>> {
        if ix >= self.len() {
            return None;
        }
        match self.ix.get_step(ix) {
            Ok(six) => {
                debug_assert!(six <= ix);
                debug_assert!(ix <= self.len());
                self.elems.get(six).map(Some)
            }
            Err(_) => Some(None),
        }
    }

    /// Get an element of this stack, or the number of elements before it
    #[inline]
    pub fn get_pos(&self, ix: usize) -> Result<&A::Item, usize> {
        match self.ix.get_step(ix) {
            Ok(six) => {
                debug_assert!(six <= ix);
                debug_assert!(ix <= self.len());
                let len = self.elems.len();
                self.elems.get(six).ok_or(len)
            }
            Err(eix) => {
                debug_assert!(eix <= ix);
                Err(eix)
            }
        }
    }

    /// Get an element of this stack, or `None`
    #[inline]
    pub fn get_mut(&mut self, ix: usize) -> Option<Option<&mut A::Item>> {
        if ix >= self.len() {
            return None;
        }
        match self.ix.get_step(ix) {
            Ok(six) => {
                debug_assert!(six <= ix);
                debug_assert!(ix <= self.len());
                self.elems.get_mut(six).map(Some)
            }
            Err(_) => Some(None),
        }
    }

    /// Get an element of this stack, or the number of elements before it
    #[inline]
    pub fn get_pos_mut(&mut self, ix: usize) -> Result<&mut A::Item, usize> {
        match self.ix.get_step(ix) {
            Ok(six) => {
                debug_assert!(six <= ix);
                debug_assert!(ix <= self.len());
                let len = self.elems.len();
                self.elems.get_mut(six).ok_or(len)
            }
            Err(eix) => {
                debug_assert!(eix <= ix);
                Err(eix)
            }
        }
    }

    /// Get the index for a given element, if any
    #[inline(always)]
    pub fn get_ix(&self, ix: usize) -> Result<usize, usize> {
        match self.ix.get_step(ix) {
            Ok(six) if six < self.elems.len() => Ok(six),
            Ok(eix) | Err(eix) => Err(eix.min(self.elems.len())),
        }
    }

    /// Get the elements of this sparse stack
    #[inline(always)]
    pub fn elems(&self) -> &[A::Item] {
        &self.elems[..]
    }

    /// Borrow the indices of this sparse stack
    #[inline(always)]
    pub fn ix(&self) -> SparseIndexRef<usize> {
        self.ix.borrowed()
    }

    /// Push an element onto this stack
    #[inline]
    pub fn push(&mut self, elem: Option<A::Item>) {
        if let Some(elem) = elem {
            self.push_some(elem)
        } else {
            self.push_none(1)
        }
    }

    /// Push a `Some` element onto this stack
    #[inline]
    pub fn push_some(&mut self, elem: A::Item) {
        let len = self.elems.len();
        self.ix.push_some(len, 1, 0);
        self.elems.push(elem);
    }

    /// Push `n` `None` elements onto this stack
    #[inline(always)]
    pub fn push_none(&mut self, n: usize) {
        self.ix.extend(n, 0);
    }

    /// Pop an element from this stack
    #[inline(always)]
    pub fn pop(&mut self) -> Option<Option<A::Item>> {
        let popped = self.ix.pop()?;
        if popped + 1 == self.elems.len() {
            let popped = self.elems.pop();
            debug_assert!(!popped.is_none());
            Some(popped)
        } else {
            debug_assert!(popped >= self.elems.len());
            Some(None)
        }
    }

    /// Get the element capacity of this sparse stack
    #[inline(always)]
    pub fn elem_capacity(&self) -> usize {
        self.elems.capacity()
    }

    /// Get the segment capacity of this sparse stack
    #[inline(always)]
    pub fn segment_capacity(&self) -> usize {
        self.ix.capacity()
    }

    /// Reserve enough space for `n` additional elements in `s` additional segments
    #[inline]
    pub fn reserve(&mut self, n: usize, s: usize) {
        self.elems.reserve(n);
        self.ix.reserve(s);
    }
}

impl<A> Default for SparseStack<A>
where
    A: Array,
{
    #[inline(always)]
    fn default() -> Self {
        Self::new()
    }
}

impl<A> Debug for SparseStack<A>
where
    A: Array,
    A::Item: Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("SparseStack")
            .field("elems", &self.elems)
            .field("ix", &self.ix)
            .finish()
    }
}

impl<A> Clone for SparseStack<A>
where
    A: Array,
    A::Item: Clone,
{
    #[inline]
    fn clone(&self) -> Self {
        Self {
            elems: self.elems.clone(),
            ix: self.ix.clone(),
        }
    }
}

impl<A, B> PartialEq<SparseStack<A>> for SparseStack<B>
where
    A: Array,
    B: Array,
    A::Item: PartialEq<B::Item>,
{
    #[inline]
    fn eq(&self, other: &SparseStack<A>) -> bool {
        self.elems.len() == other.elems.len()
            && self.ix == other.ix
            && self
                .elems
                .iter()
                .zip(other.elems.iter())
                .all(|(this, other)| *other == *this)
    }
}

impl<A> Eq for SparseStack<A>
where
    A: Array,
    A::Item: Eq,
{
}

impl<A> Hash for SparseStack<A>
where
    A: Array,
    A::Item: Hash,
{
    #[inline]
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.elems.hash(state);
        self.ix.hash(state);
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn integer_stack() {
        let mut stack = SparseStack::<[i32; 4]>::default();
        let mut state = Vec::new();
        assert_eq!(stack.len(), 0);
        assert_eq!(stack.no_elems(), 0);
        assert_eq!(stack.no_gaps(), 0);
        stack.push_some(5);
        state.push(stack.clone());
        assert_eq!(stack.len(), 1);
        assert_eq!(stack.no_elems(), 1);
        assert_eq!(stack.no_gaps(), 0);
        stack.push_some(6);
        state.push(stack.clone());
        assert_eq!(stack.len(), 2);
        assert_eq!(stack.no_elems(), 2);
        assert_eq!(stack.no_gaps(), 0);
        assert_eq!(stack.get(2), None);
        assert_eq!(stack.get(3), None);
        assert_eq!(stack.get(4), None);
        assert_eq!(stack.get_ix(2), Err(2));
        assert_eq!(stack.get_ix(3), Err(2));
        assert_eq!(stack.get_ix(4), Err(2));
        stack.push_none(3);
        state.push(stack.clone());
        assert_eq!(stack.len(), 5);
        assert_eq!(stack.no_elems(), 2);
        assert_eq!(stack.no_gaps(), 3);
        assert_eq!(stack.get(2), None);
        assert_eq!(stack.get(3), None);
        assert_eq!(stack.get(4), None);
        assert_eq!(stack.get_ix(2), Err(2));
        assert_eq!(stack.get_ix(3), Err(2));
        assert_eq!(stack.get_ix(4), Err(2));
        stack.push_some(7);
        state.push(stack.clone());
        assert_eq!(stack.get(2), Some(None));
        assert_eq!(stack.get(3), None);
        assert_eq!(stack.get(4), None);
        assert_eq!(stack.get_ix(2), Err(2));
        assert_eq!(stack.get_ix(3), Err(2));
        assert_eq!(stack.get_ix(4), Err(2));
    }
}
